var express = require("express");
var bodyParser = require("body-parser");
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var routes = require("./src/routes/routes.js")(app);

var server = app.listen(8085, function () {
    console.log("Servidor ON >> http://localhost:" + server.address().port);
});
