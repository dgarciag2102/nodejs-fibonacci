# NodeJS - Fibonacci API REST
La serie de Fibonacci se caracteriza por el hecho de que cada número en dicha serie es igual a la suma de los anteriores. Para la siguiente aplicación se recibe un índice "n" y se devuelve el valor en la serie de Fibonacci que corresponde al índice dado.

## ¿Cómo usar esta aplicación?
Como requisito se debe tener instalado NodeJS, a continuación los pasos para ejecutar la aplicación.

1. Clonar el repositorio.
```bash
git clone https://gitlab.com/dgarciag2102/nodejs-fibonacci.git
cd nodejs-fibonacci
```
2. Instalar los paquetes necesarios.
```bash
npm install
```
3. Iniciar el servidor.
```bash
node app.js
```
4. Finalmente, Hacer las solicitudes correspondientes (A través de cualquier navegador, o plataformas de gestión de API).

## ¿Qué decisiones técnicas se tomaron?
- Validación de índice enviado: Primero se evalúa la existencia del índice, a continuación si el índice enviado está vacío y finalmente si es un entero positivo mayor a 0 o una cadena de caracteres.
- Redireccionar al directorio raíz, y agregar el índice '0' para búsquedas ajenas a la aplicación.