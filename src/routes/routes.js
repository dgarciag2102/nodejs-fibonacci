var appRouter = function (app) {
	app.get("/", function (request, response) {
		if (!request.query.index) {
			if (request.query.index == "")
				return response.send({ "status": "error", "message": "El parámetro 'index' debe ser un número entero mayor a 0." });
			else
				return response.send({ "status": "error", "message": "Falta el parámetro 'index' en la solicitud." });
		}

		if (request.query.index < 0) {
			return response.send({ "status": "error", "message": "index should be positive" });
		}
		else {
			if (checkIsNan(request.query.index)) {
				return response.send({ "status": "error", "message": "El parámetro 'index' enviado no es número entero." });
			} else {
				return response.send({ "status": "success", "result": fibonacci(request.query.index) });
			}
		}
	});

	app.get("*", function (request, response) {
		response.redirect("/?index=0");
	});
}

function fibonacci(index) {
	if (index == 0) {
		return 0;
	} else if (index == 1) {
		return 1;
	} else {
		return Number(fibonacci(index - 2)) + Number(fibonacci(index - 1));
	}
}

function checkIsNan(request) {
	return isNaN(request) ? true : false;
}

module.exports = appRouter;